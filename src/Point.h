/**
 *	Point template class to represent a (X,Y) coordinate
 */
#pragma once

template< typename T >
class Point
{
public:
	T x;
	T y;

	Point(T x_, T y_)
		: x(x_), y(y_) {}

	template < typename O >
	Point(const Point<O>& p)
		: x(static_cast<T>(p.x)), y(static_cast<T>(p.y)) {}

	bool operator==(const Point<T>& p) const
	{
		return x == p.x && y == p.y;
	}
};
