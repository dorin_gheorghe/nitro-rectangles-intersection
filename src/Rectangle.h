/**
 *	Rectangle class that represents a rectangle in the cartesian system
 *	It is defined by a top left corner and bottom right points
 */
#pragma once

template <typename T>
class Rectangle
{
public:
	Point<T> topLeft;
	T width;
	T height;

	Rectangle(T left, T top, T width_, T height_)
		: topLeft(left, top)
		, width(width_)
		, height(height_)
	{
		if (width <= 0 || height <= 0)
			throw std::runtime_error("Width or height was not positive");

		width = width_;
		height = height_;
	}

	Rectangle() : Rectangle(0, 0, 1, 1) {}

	Rectangle(const Rectangle& r)
		: topLeft(r.topLeft),
		width(r.width),
		height(r.height), 
		m_indexes(r.m_indexes) {}

	Rectangle& operator=(const Rectangle& rhs)
	{
		topLeft = rhs.topLeft;
		width = rhs.width;
		height = rhs.height;
		m_indexes = rhs.m_indexes;

		return *this;
	}

	void SetRectangleIndexes(std::set<T> indexes)
	{
		m_indexes = indexes;
	}

	std::set<T> GetRectangleIndexes(void) const
	{
		return m_indexes;
	}

	bool operator==(const Rectangle& r) const
	{
		return topLeft == r.topLeft &&
			width == r.width &&
			height == r.height;
	}

	T Left() const { return topLeft.x; }
	T Top() const { return topLeft.y; }
	T Right() const { return Left() + width; }
	T Bottom() const { return Top() + height; }
	T Width() const { return width; }
	T Height() const { return height; }

private:
	std::set<T> m_indexes;
};
