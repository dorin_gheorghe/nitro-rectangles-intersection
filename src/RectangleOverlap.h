/**
 *	Class that checks for all possible rectangle overlaps from the input
 */
#pragma once

#include <vector>
#include <queue>
#include <set>
#include <sstream>
#include <algorithm>

#include "Point.h"
#include "Rectangle.h"

template<typename T>
class RectangleOverlap
{
public:
	RectangleOverlap(std::vector<Rectangle<T>> input_) : m_input(input_)
	{
		std::sort(m_input.begin(), m_input.end(),
				  [this](Rectangle<T> a, Rectangle<T> b) { return a.Left() < b.Left(); });
	}

	void DisplayAllValidInputRectangles()
	{
		std::cout << "Input:" << std::endl;

		int rectCounter = 0;
		for (const auto it : m_input)
		{
			std::cout << "\t" << (rectCounter++) + 1<< ": Rectangle at ("
						<< it.Left() << "," << it.Top() << "), "
						<< "w=" << it.Width() << ", h=" << it.Height() << std::endl;
		}
	}

	// All intersections happen at one of the top left corner
	// of a rectangle, therefore start the check on X-axis
	// to determine whether there are any intersections
	void PrintIntersections()
	{
		// Assign each rectangle an id
		for (auto i = 0; i < m_input.size(); ++i)
			m_input[i].SetRectangleIndexes(std::set<T>({ i }));

		// Start from the second element, as the first is the most left
		// sorted rectangle and it cannot overlap with any other rectangle,
		// with the exception of a rectangle starting at the same position,
		// but that will be taken into account at the next iteration
		std::queue<Rectangle<T>> queueOfAllCombinations;
		std::set<std::set<T>> checkUniqueness;
		std::vector<Rectangle<T>> singleRectangles;
		for (auto i = 1; i < m_input.size(); ++i)
		{
			for (auto j = 0; j < i; ++j)
			{
				// Check to see if this rectangle is in the same range
				// with respect to X coordinate compared to previous rectangles
				if (m_input[i].Left() >= m_input[j].Left() &&
					m_input[i].Left() <= m_input[j].Right())
				{
					singleRectangles.push_back(m_input[j]);
					// If it is, check with which one overlaps
					if (IsOverlapping(m_input[j], m_input[i]))
					{
						auto intersect = IntersectedRectangles(m_input[j], m_input[i]);
						intersect.SetRectangleIndexes(std::set<T>({ i, j }));
						// Add to the queue of rectangle combinations to be processed
						queueOfAllCombinations.push(intersect);
						checkUniqueness.emplace(std::set<T>({ i, j }));

						std::cout << "Intersection between rectangle " << j + 1 << " and " << i + 1
							<< " at (" << intersect.Left() << "," << intersect.Top()
							<< "), w=" << intersect.Width() << ", h=" << intersect.Height() << std::endl;
					}
				}
			}
			PrintAllCombinations(queueOfAllCombinations, checkUniqueness, singleRectangles);
			checkUniqueness.clear();
			singleRectangles.clear();
		}
	}

private:
    void PrintAllCombinations(std::queue<Rectangle<T>>& combinations,
        std::set<std::set<T>>& uniqueness,
        std::vector<Rectangle<T>> singleRectangles)
    {
        while (!combinations.empty())
        {
            auto top = combinations.front();
            combinations.pop();

            for (auto it : singleRectangles)
            {
                auto topIndexes = top.GetRectangleIndexes();
                auto itIndexes = it.GetRectangleIndexes();
                topIndexes.insert(itIndexes.begin(), itIndexes.end());

                // Check to see if the current rectangle was already verified
                if (std::find_if(uniqueness.begin(), uniqueness.end(),
                    [uniqueness, topIndexes](std::set<T> element){ return element == topIndexes; }) == uniqueness.end())
                {
                    if (IsOverlapping(top, it))
                    {
                        auto intersect = IntersectedRectangles(top, it);
                        intersect.SetRectangleIndexes(topIndexes);
                        // Add to the queue of rectangle combinations to be processed
                        combinations.push(intersect);
                        uniqueness.insert(topIndexes);

                        std::ostringstream os;
                        os << "Intersection between rectangle ";
                        for (const auto id : topIndexes)
                            os << id + 1 << ", ";
                        os << "at (" << intersect.Left() << "," << intersect.Top()
                            << "), w=" << intersect.Width() << ", h=" << intersect.Height();
                        std::cout << os.str() << std::endl;
                    }
                }
            }
        }
    }

	bool IsOverlapping(Rectangle<T> r1, Rectangle<T> r2)
	{
		return (r1.Left() <= r2.Right() &&
			r1.Top() <= r2.Bottom() &&
			r1.Right() >= r2.Left() &&
			r1.Bottom() >= r2.Top() );
	}

	Rectangle<T> IntersectedRectangles(Rectangle<T> a, Rectangle<T> b)
	{
		Rectangle<T> result;
		// The top left X coordinate of the rectangles intersection will be
		// the second one
		result.topLeft.x = std::max(a.Left(), b.Left());

		// Depending on the position of the two rectangles, set the top left Y
		// to the larger of rectangles Y values
		result.topLeft.y = (a.topLeft.y < b.topLeft.y) ? b.topLeft.y : a.topLeft.y;

		// Computing bottom right corner
		result.width = (a.Right() < b.Right()) ? (a.Right() - result.topLeft.x) : (b.Right() - result.topLeft.x);
		result.height = (a.Bottom() < b.Bottom()) ? (a.Bottom() - result.topLeft.y) : (b.Bottom() - result.topLeft.y);

		return result;
	}

	// The intersections may only occur at each top left point
	// of a rectangle, thus save the X coordinates of each rectangle
	// and sort them, together with the X coordinates of the bottom right point
	std::vector<Rectangle<T>> m_input;
};
