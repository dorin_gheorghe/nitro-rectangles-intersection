/**
 * Json Reader class using boost json parser
 */
 
#pragma once

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sstream>
#include <vector>

#include "Rectangle.h"

// Read no more than 1000 valid rectangles
const int maxNumberRectangles = 1000;

template< typename T >
class JsonReader
{
public:
    // TODO Dorin: make the type of file platform independent
    JsonReader(const char* filename) : m_fileName(const_cast<char*>(filename)) {}

    std::vector<Rectangle<T>> ParseAndValidateFields()
    {
        try
        {
            std::stringstream os;
            std::ifstream file(m_fileName);
            os << file.rdbuf();
	
            boost::property_tree::ptree pt;
            boost::property_tree::read_json(os, pt);

            for(auto &v : pt.get_child("rects"))
            {
                if (rectangles.size() < maxNumberRectangles)
                {
                    Rectangle<T> rect;
                    bool valid = true;
                    for (auto &cell : v.second)
                    {
                        auto fieldValue = cell.second.get_value<T>();

                        AddCoordinateToRectangle(rect, cell.first, fieldValue, valid);
                        if (!valid)
                           break;
                    }

                    if (valid)
                        rectangles.push_back(rect);
                }
                else
                	break; // Break the loop if the limit of read rectangles has been reached
            }
        }
        catch (std::exception const& e)
        {
            std::cerr << e.what() << std::endl;
            return std::vector<Rectangle<T>>();
        }

        return rectangles;
    }

private:

	void AddCoordinateToRectangle(Rectangle<T>& rect, std::string key, T value, bool& valid)
	{
		if (valid)
		{
			if (key == "x")
				rect.topLeft.x = value;
			else if (key == "y")
				rect.topLeft.y = value;
			else if (key == "w")
				value > 0 ? rect.width = value : valid = false;
			else if (key == "h")
				value > 0 ? rect.height = value : valid = false;
			else
				valid = false; // Unknown value in the row, place thee valid flag to false
		}
	}

    char* m_fileName;
    std::vector<Rectangle<T>> rectangles;
};
