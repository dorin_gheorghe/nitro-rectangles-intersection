### Nitro Rectangle Intersection assignment ###

This file is intended to explain how to build and run the source files in this repository.

1. Build
   The input json file is parsed with the help of the Boost library tools, namely PropertyTree (i.e. ptree) and json parser (i.e. json_parser)
   The 3rd party tool can be downloaded from https://sourceforge.net/projects/boost/files/boost/1.62.0/ (version 1.62.0). After download, it can be
   linked with repository source files in Visual Studio (I used 2013) project (see http://www.boost.org/doc/libs/1_62_0/more/getting_started/windows.html 
   paragraph 4.1 Build From the Visual Studio IDE).

2. Execute
   Once the build succeeds, the resulting project executable can be run at command line with at least one argument, which in this case should be the name
   of the json input file.