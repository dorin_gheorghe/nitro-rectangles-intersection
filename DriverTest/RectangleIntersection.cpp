// RectangleIntersection.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

#include "RectangleOverlap.h"
#include "JsonReader.h"

int main(int argc, char* argv[])
{
    if (argc <= 1)
    {
        std::cout << "ERROR: At least one argument must be used" << std::endl;
        exit(1);
    }

    // Read the input json file
    JsonReader<int> jsonInput(argv[1]);
    auto vec = jsonInput.ParseAndValidateFields();

    // Test with two duplicate rectangles
	// Rectangle<int> r1(100, 100, 250, 80), r2(120, 200, 250, 150), r3(140, 160, 250, 100), r4(160, 140, 350, 190), r5(100, 100, 250, 80);
	// std::vector<Rectangle<int>> vec{ r1, r2, r3, r4, r5 };

    RectangleOverlap<int> overlap(vec);
    overlap.DisplayAllValidInputRectangles();
    overlap.PrintIntersections();

	return 0;
}
